#ifndef BUFFER_MANAGER_H
#define BUFFER_MANAGER_H
 
 
#include <stdio.h>
#include <stdlib.h>
#include "buffer_mgr.h"
#include "storage_mgr.h"
 
 
// Include return codes and methods for logging errors
#include "dberror.h"
 
// Include bool DT
#include "dt.h"
 
// Replacement Strategies
typedef enum ReplacementStrategy {
    RS_FIFO = 0,
    RS_LRU = 1,
    RS_CLOCK = 2,
    RS_LFU = 3,
    RS_LRU_K = 4
} ReplacementStrategy;
 
// Data Types and Structures
typedef int PageNumber;
#define NO_PAGE -1
int NumReadIO; //Any Definition ??
int NumWriteIO;//Any Definition ??
 
typedef struct BM_BufferPool {
    char *pageFile;
    int numPages;
    ReplacementStrategy strategy;
    void *mgmtData; // use this one to store the bookkeeping info your buffer
    // manager needs for a buffer pool
    int num_of_writes; // used by FIFO
 
} BM_BufferPool;
 
typedef struct BM_PageHandle {
    PageNumber pageNum;
    char *data;
    bool dirty;// use this to indicate the page be modified or not
    int fixCount;// fixCount is based on pinning/un-pinning request
        int num_of_fixs; // For LRU Replacment Stratgy . used to count the number of clients used this page
} BM_PageHandle;
 
 
// convenience macros
#define MAKE_POOL()                 \
        ((BM_BufferPool *) malloc (sizeof(BM_BufferPool)))
 
#define MAKE_PAGE_HANDLE()              \
        ((BM_PageHandle *) malloc (sizeof(BM_PageHandle)))
 
//  ** Replacment Strategy functions ** By Awab //
// This is a FIFO replacment strategy for our buffer manager
// /FIFO (First Come First )
void FIFO(BM_BufferPool *const bm,  BM_PageHandle *const page)
{
    BM_PageHandle *getpagesfrombm = (BM_PageHandle  *) bm->mgmtData;
 
    int i, bm_front_pg;
    bm_front_pg = bm->num_of_writes % bm->numPages;
 
    // Go through Buffer Manager bm to find a victom starting from the oldest added page
    for(i = 0; i < bm->numPages; i++){
        if(getpagesfrombm[bm_front_pg].fixCount == 0)
        { //this is a victim
 
            if(getpagesfrombm[bm_front_pg].dirty == true)
            { // Data in victim changed, we need to write the change to disk
                SM_FileHandle fh;
                openPageFile(bm->pageFile, &fh);
                writeBlock(getpagesfrombm[bm_front_pg].pageNum, &fh, getpagesfrombm[bm_front_pg].data);
                NumWriteIO++;  // add 1 to the number of writes on disk
            }
            // replacing the victim whith the new page
            getpagesfrombm[bm_front_pg].pageNum = page->pageNum;
            getpagesfrombm[bm_front_pg].data = page->data;
            getpagesfrombm[bm_front_pg].dirty = page->dirty;
            getpagesfrombm[bm_front_pg].fixCount = page->fixCount;
            getpagesfrombm[bm_front_pg].num_of_fixs = page->num_of_fixs;
            break;
        }
        else
        {           // That's mean, someone using the current page. So, move to the next to find a victim
            bm_front_pg++;
            bm_front_pg = (bm_front_pg % bm->numPages == 0) ? 0 : bm_front_pg; // to go back to the begining of array
        }
    }
}
 
// Defining LRU (Least Recently Used) function
void LRU (BM_BufferPool *const bm, BM_PageHandle *const page)
{ // in this strategy we will have two for loops one is to find the first victim, then go through another loop
    // to compare this victim to other and change the victim if we find a nicer victim :) nicer=
    // few clients use this page = lower num of fixs
    BM_PageHandle *getpagesfrombm = (BM_PageHandle  *) bm->mgmtData;
    int i, least_FixsNum_page_index, least_num_of_fixs ;
 
    // find the first page which no one use. Assume it's the victim
    for(i = 0; i < bm->numPages; i++)
    {
        if(getpagesfrombm[i].fixCount == 0){
            least_FixsNum_page_index = i; //this is the victim right now
            least_num_of_fixs = getpagesfrombm[i].num_of_fixs; // store to compare in next step
            break;
        }
    }
 
    // Go through all pages to find the a best page to be victim (the one that have min num_of_fixs)
    for(i = least_FixsNum_page_index + 1; i < bm->numPages; i++){
        if(getpagesfrombm[i].num_of_fixs < least_num_of_fixs){
            least_FixsNum_page_index = i;
            least_num_of_fixs = getpagesfrombm[i].num_of_fixs;
        }
    }
 
    if(getpagesfrombm[least_FixsNum_page_index].dirty == true)
    { // Data in victim changed, we need to write the change to disk
        SM_FileHandle fh;
        openPageFile(bm->pageFile, &fh);
        writeBlock(getpagesfrombm[least_FixsNum_page_index].pageNum, &fh,
            getpagesfrombm[least_FixsNum_page_index].data);
 
        NumWriteIO++;  // add 1 to the number of writes on disk
    }
    // replacing the victim whith the new page
    getpagesfrombm[least_FixsNum_page_index].pageNum = page->pageNum;
    getpagesfrombm[least_FixsNum_page_index].data = page->data;
    getpagesfrombm[least_FixsNum_page_index].dirty = page->dirty;
    getpagesfrombm[least_FixsNum_page_index].fixCount = page->fixCount;
    getpagesfrombm[least_FixsNum_page_index].num_of_fixs = page->num_of_fixs;
 
}
 
// ******************************END OF *************************//
//   ** Replacment Strategy functions ** By Awab //
 
 
 
//  ** Buffer Manager Interface Pool Handling ** By Lily //
 
/*creates a new buffer pool with numPages page frames using the page replacement strategy strategy.
 Initially, all page frames should be empty.The page file should already exist, this method should
 not generate a new page file.stratData can be used to pass parameters for the page replacement
 strategy. For example, for LRU-k this could be the parameter k.*/
 
RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName,
                  const int numPages, ReplacementStrategy strategy,
                  void *stratData){
 
    File *fp = fopen(pageFileName,"r");
    if (fp==NULL){
        return RC_FILE_NOT_FOUND;
    }
    bm->pageFile = (char*)pageFileName;
    bm->numPages = numPages;
    bm->strategy = strategy;
    BM_PageHandle *page = malloc(sizeof(BM_PageHandle) * numPages);
    bm->mgmtData = page;
        bm->num_of_writes = 0; // for FIFO repleasment method
    //intilize all pages in buffer pool
    for(int i = 0; i < numPages; i++)
    {
        page[i].pageNum = -1;
        page[i].data = NULL;
        page[i].dirty = false;
        page[i].fixCount = 0;
    }
    bm->NumReadIO = 0;
    bm->NumWriteIO = 0;
    fclose(fp);
    return RC_OK;
}
 
/*shutdownBufferPool destroys a buffer pool.This method should free
 up all resources associated with buffer pool.it should free the
 memory allocated for page frames.If the buffer pool contains any
 dirty pages, then these pages should be written back to disk before
 destroying the pool.It is an error to shutdown a buffer pool that has
 pinned pages.*/
 
RC shutdownBufferPool(BM_BufferPool *const bm){
    BM_PageHandle *page = (BM_PageHandle *)bm->mgmtData;
    //maximum number of page frames that can be kept into the buffer pool
    int numPages = bm->numPages;
    bool isDirty =false;
    for(int i=0; i < numPages; i++){
        if(page[i].fixCount != 0)){
            //It is an error to shutdown a buffer pool that has pinned pages.
            printf("Shutdown failed\n", );
        }
        if(page[i].dirty == true){
            isDirty = true;
        }
    }
    if(isDirty == true){
        forceFlushPool(bm);
    }
    free(page);
    bm->mgmtData = NULL;
    return RC_OK;
}
 
/*forceFlushPool causes all dirty pages (with fix count 0) from the
 buffer pool to be written to disk.*/
 
RC forceFlushPool(BM_BufferPool *const bm){
    BM_PageHandle *page = (BM_PageHandle *)bm->mgmtData;
    //maximum number of page frames that can be kept into the buffer pool
    int numPages = bm->numPages;
    //store all dirty pages to be written to disk
    for(int i=0; i< numPages; i++){
        if(page[i].fixCount == 0 && page[i].dirty == true)
        {
            SM_FileHandle fh;
            openPageFile(bm->pageFile, &fh);
            writeBlock(page[i].pageNum, &fh, page[i].data);
            page[i].dirty = false;
        }
    }
    return RC_OK;
}
 
// ******************************END OF *************************//
//  ** Buffer Manager Interface Pool Handling ** By Lily //
 
 
 
//  ** Buffer Manager Interface Access Pages ** By Sawsane //
 
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page){
    page->dirty = true;
    return RC_OK;
 
}
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
    page->fix_count --;
    return RC_OK;
 
}
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page){
    //write page to disk
    SM_FileHandle fh;
    openPageFile(bm->pageFile, &fh);
    writeBlock(page->pageNum, &fh, page->data);
    page->dirty = false;
    return RC_OK;
 
}
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
    //check if page is already requested by another client
    PageNumber ptr_frame;
 
    if(page->dirty){//if the page is dirty
        //pointer to the frame where the page resides
        ptr_frame = page->pageNum;
    }else{
        //read page from disk
        //readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
        SM_FileHandle fh;
        SM_PageHandle mempage;
        readBlock(page->pageNum, &fh, mempage);
        //place the page in the frame (replacement strategy)
        if(bm->strategy == 0){
            FIFO (bm, page);//0
        }else if(bm->strategy == 1){
            LRU (bm, page);//1
        }
        //other replacement strategies if any
 
        //pointer to the frame where the page resides now
        ptr_frame = page->pageNum;
    }
    //increment the fix count
    page->fix_count ++;
 
    return ptr_frame ;
 
}
// ******************************END OF *************************//
//  ** Buffer Manager Interface Access Pages ** By Sawsane //
 
 
 
//  ** Statistics Interface ** By Yitao //
 
 
int CURR_POS; //Any Definition ??
 
#define FIX_PAGE 0; //Any Definition ??
 
PageNumber *getFrameContents (BM_BufferPool *const bm)
{
    BM_PageHandle *getframecontents = (BM_PageHandle  *) bm->mgmtData;
 
    // the maximum size of buffer pool
    int MAX_numPage = bm -> numPages;
    // create an array
    int *FC_array = malloc(MAX_numPage * sizeof(int));
 
    for (CURR_POS = 0; CURR_POS < MAX_numPage; CURR_POS++)
    {
        if(getframecontents[CURR_POS].pageNum == NO_PAGE)
        {
            FC_array[CURR_POS] = NO_PAGE;
            printf("Page error!");
        }
        else
            FC_array[CURR_POS] = getframecontents[CURR_POS].pageNum;
 
    }
    return FC_array;
};
 
 
bool *getDirtyFlags (BM_BufferPool *const bm)
{
    BM_PageHandle *getdirtyflags = (BM_PageHandle  *) bm->mgmtData;
 
    // the maximum size of buffer pool
    int MAX_numPage = bm -> numPages;
    // create an array
    bool *DF_array = malloc(MAX_numPage * sizeof(bool));
 
    for (CURR_POS = 0; CURR_POS < MAX_numPage; CURR_POS++)
    {
        if(getdirtyflags[CURR_POS].pageNum == 0)
        {
            DF_array[CURR_POS] = FALSE;
            printf("Page error!");
        } else
            DF_array[CURR_POS] = TRUE;
    }
    return DF_array;
};
 
 
int *getFixCounts (BM_BufferPool *const bm)
{
    BM_PageHandle *getfixcounts = (BM_PageHandle  *) bm->mgmtData;
 
    // the maximum size of buffer pool
    int MAX_numPage = bm -> numPages;
    // create an array
    int *FCO_array = malloc(MAX_numPage * sizeof(int));
 
    for (CURR_POS = 0; CURR_POS < MAX_numPage; CURR_POS++)
    {
        if(getfixcounts[CURR_POS].pageNum == NO_PAGE)
        {
            FCO_array[CURR_POS] = FIX_PAGE;
            printf("Page error!");
        }
        else
            FCO_array[CURR_POS] = getfixcounts[CURR_POS].pageNum;
    }
    return FCO_array;
};
 
int getNumReadIO (BM_BufferPool *const bm)
{
    return NumReadIO;
};
 
 
int getNumWriteIO (BM_BufferPool *const bm)
{
    return NumWriteIO;
 
};
// ******************************END OF *************************//
//  ** Statistics Interface ** By Yitao //
 
 
 
 
#endif