#include "dberror.h"
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include "storage_mgr.h"

FILE *pageFile;

/************************************************************
 *                manipulating page file                    *
 ************************************************************/

/* initStorageManager: initializeds the storafe manager */
void initStorageManager (void){
    printf("Initialzing the StorageManager!\n");
}

/* createPageFile */
/* This method creates a new page file fileName. The initial file size should be one page */
/* This method should fill this single page with '\0' bytes */
RC createPageFile (char *fileName){
    FILE *fp;
    fp = fopen(fileName,"w");
    if(fp==NULL)
        return RC_FILE_NOT_FOUND;
    char page[PAGE_SIZE]={0};
    fwrite(page, PAGE_SIZE,1,fp);
    fclose(fp);
    return RC_OK;

}

/* openPageFile: open an existing page file */
RC openPageFile (char *fileName, SM_FileHandle *fHandle){
    FILE *fp;
    fp = fopen(fileName, "r");
    if(fp==NULL)
        return RC_FILE_NOT_FOUND;
    int fileLength;
    fHandle->fileName=fileName;
    fHandle->curPagePos=0;
    fseek(fp,0,SEEK_END);
    fileLength=(int)ftell(fp);
    rewind(fp);
    fHandle->totalNumPages=fileLength/PAGE_SIZE;
    fHandle ->mgmtInfo=fp;
    fclose(fp);
    return RC_OK;

}

/* closePageFile: close an open page file */
RC closePageFile (SM_FileHandle *fHandle){
    fHandle->fileName = "";
    fHandle->curPagePos = 0;
    fHandle->totalNumPages = 0;
    return RC_OK;
}

/* destroyPageFile: destory(delete) a page file */
RC destroyPageFile (char *fileName){
    if(remove(fileName)==0)
        return RC_OK;
    else return RC_FILE_NOT_FOUND;
}

/******************************************************************
 *                    Reading blocks from disc                    *
 ******************************************************************/

/*
 * Function for getting block position.
 * Check if the file has less than pageNum pages or more than totalNumPages first.
 * If it does,the method should return RC_READ_NON_EXISTING_PAGE.
 */
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    FILE *fp;
    fp = fopen(fHandle -> fileName, "r");

    if(fp == NULL)
        return RC_FILE_NOT_FOUND;
    else if (pageNum > fHandle -> totalNumPages || pageNum < 0)
        return RC_READ_NON_EXISTING_PAGE;
    else
    {
        long offset = pageNum * PAGE_SIZE;
        if(fseek(fp, offset, 0) != 0)
            return RC_READ_NON_EXISTING_PAGE;
        else
            fread(memPage, sizeof(char), PAGE_SIZE,fp);
            fclose(fp);
            return RC_OK;
    }
}

// Function for getting block position.
// curPagePos means the current block position, so just return it.
extern int getBlockPos (SM_FileHandle *fHandle)
{
    return fHandle -> curPagePos;
}

// Function for reading first block.
// Set page number to 0 to read the first block.
extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(0, fHandle, memPage);
}

/* Function for reading previous block.
 * If the user tries to read a block before the first page of the file,
 * then the method return RC_READ_NON_EXISTING_PAGE.
 */
extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    int get_Previous_pageNum = fHandle -> curPagePos - 1;
    if (get_Previous_pageNum < 0)
    {
        printf("There is no previous block!");
        return RC_READ_NON_EXISTING_PAGE;
    }
    else
        return readBlock(get_Previous_pageNum, fHandle, memPage);

}

// Function for reading current block.
extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    int getpageNum = fHandle -> curPagePos;
    return readBlock(getpageNum, fHandle, memPage);
}

/* Function for reading next block.
 * If the user tries to read a block after the last page of the file,
 * then the method return RC_READ_NON_EXISTING_PAGE.
 */
extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    {
        int get_next_pageNum = fHandle -> curPagePos + 1;
        if (get_next_pageNum> PAGE_SIZE)
        {
            printf("There is no more blocks to read!");
            return RC_READ_NON_EXISTING_PAGE;
        }
        else
            return readBlock(get_next_pageNum, fHandle, memPage);
    }
}

// Function for reading last block.
extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    int get_last_pageNum =  fHandle -> totalNumPages - 1;
    if (get_last_pageNum < 0)
    {
        printf("Page number error, read the first block...");
        return readBlock(0, fHandle, memPage);
    }
    else
        return readBlock(get_last_pageNum, fHandle, memPage);
}
/************************************************************
 *                    Write Functions                       *
 ************************************************************/

extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
	//Write a page to disk using an absolute position.
	//absolute
	//check capacity (space) before writing 
	ensureCapacity (pageNum, fHandle);	
	
	FILE *fp;
	RC rc;

	fp=fopen(fHandle->fileName,"wb");
	if(fseek(fp,pageNum * PAGE_SIZE, SEEK_SET) != 0){ //
		rc = RC_READ_NON_EXISTING_PAGE;	
	} else if (fwrite(memPage, PAGE_SIZE, 1, fp) != 1){ //
		rc = RC_WRITE_FAILED; 
	} else {
		//update current position of page
		fHandle->curPagePos=pageNum;
		rc = RC_OK;
	}
	fclose(fp);

	return rc;

}
extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	//Write a page to disk using the current position position.
	//current 
	int rc ;

	if(fHandle == NULL){
		rc = RC_FILE_HANDLE_NOT_INIT;
	} 
	if(fHandle->curPagePos < 0){
		rc = RC_WRITE_FAILED;
	}

	rc =  writeBlock(fHandle->curPagePos, fHandle, memPage);

	return rc;


}
extern RC appendEmptyBlock (SM_FileHandle *fHandle){
	//Increase the number of pages in the file by one. The new last page should be filled with zero bytes.
	//1. get number of pages in the file
	//char *filename;
	int file_size;
	int new_size;
	RC rc;

	FILE *fp = fopen(fHandle->fileName, "wb");
	fseek(fp, 0, SEEK_END); // seek to end of file
	file_size = ftell(fp); // get current file pointer

	//2. new_size = original size + 1 page
	new_size = file_size + PAGE_SIZE; 
	//3. truncate
	rc = truncate(fHandle->fileName, new_size);

	return rc;

}
extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle){	//truncate 
	//If the file has less than numberOfPages pages then increase the size to numberOfPages.

	//char *filename;//file for which we check the capacity
	char *backup;
	int file_size;
	int err;
	RC rc;
	FILE *fp = fopen(fHandle->fileName, "wb");
	//need to check for errors

	fseek(fp, 0, SEEK_END); // seek to end of file
	file_size = ftell(fp); // get current file pointer
	if(file_size < PAGE_SIZE){
		//needs adjustment by increasing size of file to (page size)
		err = truncate(fHandle->fileName,PAGE_SIZE);
		if(err == 1){
			//error in truncating
			rc = RC_WRITE_FAILED;
		}else{//truncate if successful
			rc = RC_OK;
		}
	}else{
		//no adjustment needed
		//capacity is enough 
		rc = RC_OK;
	}
	fclose(fp);
	
	return rc; 
}
