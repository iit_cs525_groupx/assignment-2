#Storage Manager
--------------------------------------------------------------------------------------
##Members
--------------------------------------------------------------------------------------
- [Abdulwahab Alothaim](aalothaim@hawk.iit.edu)
- [Yitao Ma](yma48@hawk.iit.edu)
- [Li Huang](lhuang29@hawk.iit.edu)
- [Sawsane Ouchtal](souchtal@hawk.iit.edu)

---------------------------------------------------------------------------------------
##File list 
---------------------------------------------------------------------------------------
  - dberror.c
  - dberror.h
  - buffer_mgr.c
  - buffer_mgr.h
  - buffer_mgr_stat.c
  - buffer_mgr_stat.h
  - dt.h
  - storage_mgr.c
  - storage_mgr.h
  - test_assign1_1.c
  - test_assign2_1.c
  - test_helper.h
  - README.txt
  - Makefile
  
  
----------------------------------------------------------------------------------------
##Installation instruction
----------------------------------------------------------------------------------------
using test_assign1_1.c and test_assign2_1.c to test

after test, use clean to delete files except source code
 




-----------------------------------------------------------------------------------------
##Function descriptions
------------------------------------------------------------------------------------------
###Replacement Strategy Fuctions
-------------------------------------
`FIFO()` 

`LRU()` 

---------------------------------------
###Buffer  Pool Functions
-------------------------------------
`initBufferPool()` creates a new buffer pool with numPages page frames using the page replacement strategy strategy. Initially, all page frames should be empty.The page file should already exist, this method should not generate a new page file.stratData can be used to pass parameters for the page replacement strategy. For example, for LRU-k this could be the parameter k.

`shutdownbufferPool()` destroys a buffer pool.This method should free up all resources associated with buffer pool.it should free the memory allocated for page frames.If the buffer pool contains any dirty pages, then these pages should be written back to disk before destroying the pool.It is an error to shutdown a buffer pool that has pinned pages.

`forceFlushPool()`  causes all dirty pages (with fix count 0) from the buffer pool to be written to disk.

---------------------------------------
###Page Management  Functions
---------------------------------------

`markDirty()` mark a page as dirty

`unpinPage()` unpin a page indicate by "page"

`pinPage()` pins the page with page number pageNum. The buffer manager is responsible to set the pageNum field of the page handle passed to the method. Similarly, the data field should point to the page frame the page is stored in (the area in memory storing the content of the page).

`forcePage()` write the current content of the page back to the page file on disk.

---------------------------------------
###Statistics Fuctions
---------------------------------------

`getFrameContents()` returns an array of PageNumbers (of size numPages) where the ith element is the number of the page stored in the ith page frame. 

`getDirtyFlags()` returns an array of bools (of size numPages) where the ith element is TRUE if the page stored in the ith page frame is dirty. Empty page frames are considered as clean.

`getFixCounts()` returns an array of ints (of size numPages) where the ith element is the fix count of the page stored in the ith page frame. Return 0 for empty page frames.

`getNumReadIO()`  returns the number of pages that have been read from disk since a buffer pool has been initialized. ge frame

`getNumWriteIO()` returns the number of pages written to the page file since the buffer pool has been initialized.


-------------------------------------------------------------------------------------------------
##Test cases: 
-------------------------------------------------------------------------------------------------
test_assign2_1.c

This file implements several test cases using the buffer_mgr.h interface using the FIFO strategy. Please let your make file generate a test_assign2_1 binary for this code. You are encouraged to extend it with new test cases or use it as a template to develop your own test files.

test_assign2_2.c

This file implements several test cases using the buffer_mgr.h interface. Please let your make file generate a test_assign2_2 binary for this code. This test also tests the LRU strategy. You are encouraged to extend it with new test cases or use it as a template to develop your own test files.





