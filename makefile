# makefile for IIT-CS525 Assignment 1
#  Summer '18

# Global variables
CC = gcc
CFLAGS  = -Wall -g
LIBS=-lm

# Default command
default: test_str_mgr

test_str_mgr: test_assign1_1.o storage_mgr.o dberror.o
	$(CC) $(CFLAGS) -o test_str_mgr test_assign1_1.o storage_mgr.o dberror.o $(LIBS)

# use to clean an exiting built tree
.PHONY: clean

clean:
	rm *.o test_str_mgr